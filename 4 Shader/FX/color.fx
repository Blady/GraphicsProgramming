//***************************************************************************************
// color.fx by Frank Luna (C) 2011 All Rights Reserved.
//
// Transforms and colors geometry.
//***************************************************************************************
float    timeInSeconds = 50;
cbuffer cbPerObject
{
	float4x4 gWorldViewProj; 
};

struct VertexIn
{
	float3 PosL  : POSITION;
    float4 Color : COLOR;
};

struct VertexOut
{
	float4 PosH  : SV_POSITION;
    float4 Color : COLOR;
};

float r(float2 n)
{
    return frac(cos(dot(n,float2(36.26,73.12)))*354.63);
}
float noise(float2 n)
{
    float2 fn = floor(n);
    float2 sn = smoothstep(float2(0,0),float2(1,1),frac(n));
    
    float h1 = lerp(r(fn),r(fn+float2(1,0)),sn.x);
    float h2 = lerp(r(fn+float2(0,1)),r(fn+float2(1,1)),sn.x);
    return lerp(h1,h2,sn.y);
}
float value(float2 n)
{
    float total;
    total = noise(n/32.)*0.5875+noise(n/16.)*0.2+noise(n/8.)*0.1
            +noise(n/4.)*0.05+noise(n/2.)*0.025+noise(n)*0.0125;
 	return total;
}

VertexOut VS(VertexIn vin)
{
	VertexOut vout;
	float newx = value(timeInSeconds*16.0f+vin.PosL.x/4.0f);
	float newy = value(timeInSeconds*16.0f+vin.PosL.y/4.0f);
	float newz = value(timeInSeconds*16.0f+vin.PosL.z/4.0f);
	vout.Color = float4(newx,newy, newz, 1.0f);

	// Transform to homogeneous clip space.
	vout.PosH = mul(float4(vin.PosL, 1.0f), gWorldViewProj);
	
	// Just pass vertex color into the pixel shader.
    //vout.Color = k;
    
    return vout;
}

float4 PS(VertexOut pin) : SV_Target
{
    return pin.Color;
}

technique11 ColorTech
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS() ) );
		SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_5_0, PS() ) );
    }
}
