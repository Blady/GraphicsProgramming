//***************************************************************************************
// color.fx by Frank Luna (C) 2011 All Rights Reserved.
//
// Transforms and colors geometry.
//***************************************************************************************

cbuffer cbPerObject
{
	float4x4 gWorldViewProj; 
};

struct VertexIn
{
	float3 Pos   : POSITION;
	float4 Color : COLOR;
};

struct VertexOut
{
	float4 PosH  : SV_POSITION;
    float4 Color : COLOR;
	float3 Normal : NORMAL;
};

struct GeoOut
{
	float4 PosH : SV_POSITION;
	float4 Color : COLOR;
	float3 Normal : NORMAL;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout;
	
	// Transform to homogeneous clip space.
	vout.PosH = mul(float4(vin.Pos, 1.0f), gWorldViewProj);
	
	// Just pass vertex color into the pixel shader.
    vout.Color = vin.Color;
    
	vout.Normal = float3(0.0f, 1.0f, 0.0f);


    return vout;
}

void Subdivide(VertexOut vertexIn[3], out VertexOut vertexOut[6])
{
	VertexOut m[3];

	m[0].PosH = vertexIn[0].PosH + vertexIn[1].PosH;
	m[1].PosH = vertexIn[1].PosH + vertexIn[2].PosH;
	m[2].PosH = vertexIn[2].PosH + vertexIn[0].PosH;
	for(int i = 0 ; i < 3; i++)
	{
		m[i].Color = vertexIn[i].Color;
		m[i].Normal = float3(0.0f, 1.0f, 0.0f);
	}
	vertexOut[0] = vertexIn[0];
	vertexOut[1] = m[0];
	vertexOut[2] = m[2];
	vertexOut[3] = m[1];
	vertexOut[4] = vertexIn[2];
	vertexOut[5] = vertexIn[1];
}

void OutputSubdivision(VertexOut v[6], 
	inout TriangleStream<GeoOut> triStream)
{
	GeoOut gout[6];
	[unroll]
	for (int i = 0; i < 6; ++i)
	{
		gout[i].PosH = v[i].PosH;
		gout[i].Color = v[i].Color;
		gout[i].Normal = v[i].Normal;
	}

	triStream.Append(gout[0]);
	triStream.Append(gout[1]);
	triStream.Append(gout[2]);

	triStream.RestartStrip();

	triStream.Append(gout[1]);
	triStream.Append(gout[5]);
	triStream.Append(gout[3]);

	triStream.RestartStrip();

	triStream.Append(gout[2]);
	triStream.Append(gout[3]);
	triStream.Append(gout[4]);
}

[maxvertexcount(9)]
void GS(triangle VertexOut gin[3],
	inout TriangleStream<GeoOut> triStream)
{
	VertexOut v[6];
	Subdivide(gin, v);
	OutputSubdivision(v, triStream);
}

float4 PS(GeoOut pin) : SV_Target
{
	return pin.Color;
}

technique11 ColorTech
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS() ) );
		SetGeometryShader( CompileShader( gs_5_0, GS() ) );
        SetPixelShader( CompileShader( ps_5_0, PS() ) );
    }
}

